from setuptools import find_packages, setup

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

PACKAGE = "EmailListener"

classifiers = [
        "Development Status :: 5 - Production/Stable",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS independent"
]

install_requires = [
    "google-api-python-client",
    "google-auth-httplib2",
    "google-auth-oauthlib",
    "oauth2client",
]

setup(
    name=PACKAGE,
    version="1.0.0",
    description="Common module for Gmail API",
    install_requires=install_requires,
    packages=find_packages(),
    url="https://bitbucket.org/yfernandes/robotframework-commons/src/master",
    author="Prime Control",
    author_email="yuri.fernandes@primecontrol.com.br",
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=classifiers,
)